<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;
    public function unit() {
        return $this->belongsTo(Unit::class);
    }

    public function group() {
        return $this->belongsTo(Group::class);
    }

    public function overtimes() {
        return $this->hasMany(Overtime::class);
    }

    public function vacations() {
        return $this->hasMany(Vacation::class);
    }

    public function salaries() {
        return $this->hasMany(Salary::class);
    }
}
